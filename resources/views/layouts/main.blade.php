<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.header')
<style>
.table td,
.table th {
    font-size: 14px;
    border-top-width: 0px;
    border-bottom: 1px solid;
    border-color: #ebedf2 !important;
    padding: 10px 15px !important;
    height: 60px;
    vertical-align: middle !important;
}

    .select2 {
    width:100%!important;
    }
    
    textarea {
    resize: vertical;
    }
    .form-controls {
        font-size: 14px;
        border-color: #ebedf2;
        padding: .6rem 1rem;
    }        
    .centerr {
    margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%) }
    
    .scroll {
        max-height: 510px;
        overflow-y: auto;
    }    
</style>    

</head>

<body>
    <div class="wrapper">
        @include('layouts.navbar')
        @include('layouts.sidebar')

        <!-- Main Content -->
        <div class="main-panel">

            <!-- Content -->
            <div class="content mb-5" id="particles-js" style="position: absolute; max-height: 630px;">
                @yield('content')                
            </div>

            @include('layouts.footer')
        </div>s
        
    </div>
</body>

@include('layouts.script')

@stack('scripts')
</html>