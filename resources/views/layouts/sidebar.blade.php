@php
$data_session = Session::get('user_data');
$namaUser = $data_session['nama_pegawai'];
$bagian = $data_session['bagian'];
$level = $data_session['level'];
$nip = $data_session['nip'];
@endphp
        <div class="sidebar sidebar-style-2" data-background-color="white">
            <div class="sidebar-wrapper scrollbar scrollbar-inner">
                <div class="sidebar-content">                    
                    <ul class="nav nav-primary">
                        <li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">MENU</h4>
                            <hr>
						</li>                        
                        @php
                            $data_session = Session::get('user_data');
                            $level = $data_session['level'];
                            $dataSideMenu = DeHelper::sideMenu($level);
                        @endphp

                        @foreach ($dataSideMenu as $item)
                            {!!$item!!}
                        @endforeach                        
                    </ul>
                </div>
            </div>
        </div>

        <!-- End Sidebar -->