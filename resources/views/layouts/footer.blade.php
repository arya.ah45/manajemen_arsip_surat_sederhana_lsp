<!-- Footer -->
<footer class="footer bg-grey2" style="position: fixed; bottom: 0; width: 100%;">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul class="nav">
                <li class="nav-item">
                    2021 | made with <i class="fa fa-heart heart text-danger"></i> by Arya Hafizh Tofani
                </li>
            </ul>
        </nav>
    </div>
</footer>