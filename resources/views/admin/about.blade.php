@extends('layouts.main')

@section('content')

@php
$data_session = Session::get('user_data');
$nip = $data_session['nip'];
$level = $data_session['level'];   
@endphp

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div class="col-12">
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5 pb-0 w-100" style="position: absolute;">
    <div class="row mt--2">
        <div class="col-md-12">  

            <div class="card">
                <div class="card-body scroll">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="pb-2 fw-bold">About</h2>                            
                        </div>
                        <div class="col-md-3 mb-2">
                            <img src="{{asset('assets/img/hfz.jpg')}}" width="100%" alt="" srcset="">
                        </div>
                        <div class="col-md-9 mb-2">
                            <table style="width: 100%">
                                <tr>
                                    <td colspan="3">Aplikasi ini dibuat oleh:</td>
                                </tr>
                                <tr>
                                    <td width="20%">Nama</td>
                                    <td width="3%">:</td>
                                    <td width="77%">ARYA HAFIZH TOFANI</td>
                                </tr>
                                <tr>
                                    <td width="10%">NIM</td>
                                    <td width="3%">:</td>
                                    <td width="87%">1931713021</td>
                                </tr>
                                <tr>
                                    <td width="10%">Tanggal</td>
                                    <td width="3%">:</td>
                                    <td width="87%">22-11-2021</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">

    $(document).on('submit','#fr_surat',function (event) {
        event.preventDefault();
        var formData = new FormData(this);

        swal({
            title: 'Apakah anda yakin menyimpan data ini?',
            text: "Pastikan data yang dimasukkan valid!",
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Ya!',
                    className: 'btn btn-success'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Simpan) => {
            if (Simpan) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });
                $.ajax({
                    url: "{{route('simpan_surat')}}", // your request url
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (data) {
                        console.log(data);

                        if (data.kode == 1) {
                            swal({
                                position: 'center',
                                icon: 'success',
                                title: data.messages,
                                showConfirmButton: true,
                            }).then((Simpan) => {
                                if (Simpan) {
                                    window.location.href = "{{ route('surat_home') }}";
                                } else {
                                    swal.close();
                                }
                            });

                        }else{
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                title: data.messages,
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            } else {
                swal.close();
            }
        });   

    });

$(document).on('click','#btnSave',function(e){
 
});

$(document).on('change','#nama_file',function(e){
    var isi = e.target.files[0];
    console.log(isi);
    $('#judul_file').val(isi.name); 
})


$(document).ready(function () {

    $('#kategori').select2({
        placeholder: "Pilih Kategori Arsip Surat...",
        minimumInputLength: 0,
        ajax: {
            url: "{{route('select2Kategori')}}",
            dataType: 'json',
            data: function(params) {
                console.log(params);
                return {
                    q: $.trim(params.term),
                };
            },
            processResults: function(data) {
                
                var results = [];
                $.each(data, function(index, item) {
                    results.push({
                        //   id:item.id,
                        id: item.id,
                        text: item.nama,
                    });
                });
                return {
                    results: results
                };

            },
            cache: true
        }

    });    

    var table = $('#table_surat').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollCollapse": true,
        ajax: "{{ route('dataTable_surat') }}",
        "columnDefs": [
                {"className": "text-center", "targets": "_all"}
            ],        
        columns: [
            {
                data: 'no_surat',
                name: 'no_surat',
                searchable : false,
                orderable:true
            }, {
                data: 'nama_kategori',
                name: 'nama_kategori',
                orderable:true
            }, {
                data: 'judul',
                name: 'judul',
                orderable:true
            }, {
                data: 'tgl_arsip',
                name: 'tgl_arsip',
                orderable:true
            }, {
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            },
        ]
    });
});

$('#btnTolak').click(function(e) {
    e.preventDefault()    
    // console.log($('#formTolakPengajuan').serialize());

    toastr.options = {
        iconClasses: {
            success: 'toast-success'
        },
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    $.ajax({
        data: $('#formTolakPengajuan').serialize(),
        url: "",
        type: "POST",
        dataType: 'json',
        success: function(data) {
            console.log(data);
            toastr.success(data);
            location.reload(true);
        },
        error: function(data) {
            console.log('Error:', data);
        //$('#modalRPendidikan').modal('show');
        }
    });
});



$('.btnSetuju').click(function(e) {
    toastr.options = {
        iconClasses: {
            success: 'toast-success'
        },
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var data = {
        _token: "{{ csrf_token() }}",    
        id_pengajuan:$(this).data('id_pengajuan'),
        nip_admin:$(this).data('nipadmin')
        }

    $.ajax({
        data: data,
        url: "",
        type: "POST",
        dataType: 'json',
        success: function(data) {
            console.log(data);
            toastr.success(data);
            location.reload(true);

        },
        error: function(data) {
            console.log('Error:', data);
        //$('#modalRPendidikan').modal('show');
        }
    });
});

$(document).on('click','.btnOpenTolak',function(){ 
    $('#id_pengajuan').val($(this).data('id_pengajuan'));
    $('#nip_admin').val($(this).data('nipadmin'));
    $('#organisasi_ditolak').modal('show');
});

function replace(params) {
    return (params.replace(' ','')).replace(' ','')
}
</script>
@endpush