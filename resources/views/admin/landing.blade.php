@extends('layouts.main')

@section('content')

@php
$data_session = Session::get('user_data');
$nip = $data_session['nip'];
$level = $data_session['level'];   
@endphp

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div class="col-12">
                <h2 class="text-white pb-2 fw-bold">Arsip Surat</h2>
                <p class="text-white">Berikut ini adalah surat-surat yang telah terbit dan diarsipkan.
                <br>Klik <b>"Lihat"</b> pada kolom aksi untuk menampilkan surat.</p>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5 pb-0 w-100" style="position: absolute;">
    <div class="row mt--2">
        <div class="col-md-12">  

            <div class="card">
                <div class="card-body scroll">
                    <div class="table-responsive">
                        <table id="table_surat" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th>Nomor Surat</th>
                                    <th>Kategori</th>
                                    <th>Judul</th>
                                    <th>Waktu Pengarsipan</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <a class="btn btn-sm btn-primary ml-3" href="{{url('surat/create')}}"><i class="far fa-cross"></i>&nbsp;Arsipkan Surat..</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalgejala" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="title"></span>                    
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formGejala"  name="formGejala">
                    @csrf
                    <input id="id_gejala" hidden type="text" name="id_gejala" class="form-control" value="">
                    <div class="row">
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-group-default">
                                <label>Kode Gejala</label>
                                <input id="kode_gejala" name="kode_gejala" type="text" class="form-control"
                                    placeholder="Contoh: G001">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-bd">
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
                </div>
            </form>            
        </div>
    </div>
</div>

{{-- Modal TOLAK Pengajuan --}}
<div class="modal fade " id="pengajuan_ditolak" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog " style="width: 300px;" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Catatan:</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="formTolakPengajuan" name="formTolakPengajuan">
            <div class="modal-body">
            @csrf
            <input hidden name="id_pengajuan" id="id_pengajuan">
            <input hidden name="nip_admin" id="nip_admin" value="{{$nip}}">
            <div class="form-group row">
                <div class="col-md-12">
                    <label class="form-control-label mb-0" for="NamaUniv"></label>
                    <textarea class="form-control" name="catatan" id="catatan" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 100px;"></textarea>
                    <div class="invalid-feedback">*wajib diisi</div> 
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="btnTolak" class="btn btn-primary">Simpan </button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
var table

$(document).ready(function () {
    table = $('#table_surat').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollCollapse": true,
        ajax: "{{ route('dataTable_surat') }}",
        "columnDefs": [
                {"className": "text-center", "targets": "_all"}
            ],        
        columns: [
            {
                data: 'no_surat',
                name: 'no_surat',
                searchable : false,
                orderable:true
            }, {
                data: 'nama_kategori',
                name: 'nama_kategori',
                orderable:true
            }, {
                data: 'judul',
                name: 'judul',
                orderable:true
            }, {
                data: 'tgl_arsip',
                name: 'tgl_arsip',
                orderable:true
            }, {
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            },
        ]
    });
});

    $(document).on('click','.deleteData',function (event) {
        var id = $(this).data('id');
        console.log(id);
        swal({
            title: 'Apakah anda yakin ingin menghapus arsip surat ini?',
            text: "data yang dihapus tidak dapat dikembalikan!",
            type: 'warning',
            icon: 'warning',
            buttons: {
                confirm: {
                    text: 'Ya!',
                    className: 'btn btn-success'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Simpan) => {
            if (Simpan) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });
                $.ajax({
                    url: "{{url('surat/delete')}}/"+id, // your request url
                    processData: false,
                    contentType: false,
                    type: 'GET',
                    success: function (data) {
                        console.log(data);
                        table.ajax.reload();
                        if (data.kode == 1) {
                            swal({
                                position: 'center',
                                icon: 'success',
                                title: data.messages,
                                showConfirmButton: false,
                                timer: 1500
                            });

                        }else{
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                title: data.messages,
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            } else {
                swal.close();
            }
        });   

    });

</script>
@endpush