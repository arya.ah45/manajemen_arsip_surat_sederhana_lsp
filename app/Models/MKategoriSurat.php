<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MKategoriSurat extends Model
{
    use Columns;

    public function surat() {
        return $this->hasMany(Surat::class, 'id_kategori');
    }    
}
