<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    use Columns;

    public function tagihan() {
        return $this->belongsTo(MKategoriSurat::class, 'id_kategori');
    }    
}
