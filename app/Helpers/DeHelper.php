<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;


class DeHelper
{

    public static function format_tanggal($tgl)
    {

        $bln = date("m", strtotime($tgl));
        $bln_h = array('01' => "Januari", "02" => "Februari", "03" => "Maret", "04" => "April", "05" => "Mei", "06" => "Juni", "07" => "Juli", "08" => "Agustus", "09" => "September", "10" => "Oktober", "11" => "Nopember", "12" => "Desember");
        $bln = $bln_h[$bln];
        $tg = date("d", strtotime($tgl));
        $thn = date("Y", strtotime($tgl));
        $print = $tg.' '.$bln.' '.$thn;

        return $print;
    }

    public static function sideMenu($level)
    {
            $surat = (request()->is('*surat*')) ? 'active' : '';
            $master = (request()->is('*master/*')) ? 'active' : '';
            $about = (request()->is('*about/*')) ? 'active' : '';

            $link_surat = route('surat_home');
            $link_about = route('about');

            $rule = [
                'admin' => [0,1],
                'kabag' => [2,6],
                'verifikator' => [5]
            ];

            $sideMenu = array(
                '0' =>'<li class="nav-item '.$surat.' ">
                            <a href="'.$link_surat.'">
                                <i class="fas fa-star"></i>
                                <p>Arsip</p>
                            </a>
                        </li>',       
                '1' =>'<li class="nav-item '.$about.' ">
                            <a href="'.$link_about.'">
                                <i class="fas phpdebugbar-fa-info-circle"></i>
                                <p>About</p>
                            </a>
                        </li> ',       
            );

            foreach ($rule[$level] as $key => $value) {
                $menu[$key] = $sideMenu[$value];
            }
            return $menu;
    }

    public static function status($ket)
    {
        $status = array(
            '0' =>'<span class="badge badge-secondary">Diperbaiki</span>',
            '1' =>'<span class="badge badge-secondary">Draf</span>',
            '2' =>'<span class="badge badge-primary">Disetujui Admin</span>',
            '3' =>'<span class="badge badge-danger">Ditolak Admin</span>',
            '4' =>'<span class="badge badge-danger">Mengantri</span>',
            '5' =>'<span class="badge badge-warning">Diperbaiki</span>',
            '6' =>'<span class="badge badge-success">Selesai</span>'
        );
        if (is_numeric($ket)) {
            if (isset($status[$ket])) {
                $data = $status[$ket];
            } else {
                $data = $status[$ket];
            }
        }else{
            $data = $status;
        }
        return $data;
    }
    
    public static function singkatNama($str)
    {
        $namaDepan = array_slice(explode(' ', $str), 0, 1);

        $totalNama = count(explode(' ', $str)); 

        $namaBelakang = array_slice(explode(' ', $str), -($totalNama)+1);

        foreach ($namaBelakang as $key => $value) {
            $nama[$key] = strtoupper(substr($namaBelakang[$key],0,1)).'. ';
        }
        
        return implode(' ',array_merge($namaDepan,$nama));
    }

	public static function tanggal_indonesia($tgl, $tampil_hari=true){
		$nama_hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
		$nama_bulan = array(1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		$tahun = substr($tgl, 0, 4);
		$bulan = $nama_bulan[(int)substr($tgl,5,2)];
		$tanggal = substr($tgl, 8,2);

		$text="";
		if($tampil_hari){
			$urutan_hari = date('w', mktime(0,0,0, substr($tgl, 5,2), $tanggal, $tahun));
			$hari = $nama_hari[$urutan_hari];
			$text .= $hari.", ";
		}
		$text .= $tanggal ." ". $bulan ." ". $tahun;
		return $text;
    }    
    
    public static function uang($nominal){
        if(is_numeric($nominal)){

            if(floor($nominal) != $nominal){
                $nominal = number_format($nominal, 2, ',', '.');
            }else{
                $nominal = number_format($nominal, 0, ',', '.');
            }

        }else{
            $nominal = 0;
        }

        return $nominal;
    }    
}
