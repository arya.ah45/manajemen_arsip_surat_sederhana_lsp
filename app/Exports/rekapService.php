<?php

namespace App\Exports;

use App\rekap;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class rekapService implements FromView
{
    public function view(): View
    {

        $dataRekaps = DB::table('rekaps')
                // ->where([
                //     ['tanggal_selesai','like','%'.$tanggal.'%']
                // ])
                ->orderBy('kendaraan','asc')
                ->get();   
            
            // $dataDetails = DB::table('detail_service as ds')
            //                 ->leftJoin('m_service as ms','ms.id','=','ds.id_jenis_service')
            //                 ->where('id_rekap','=',16)
            //                 // ->selectRaw('ds.id as id_rekap, tanggal_pengajuan, nama_pegawai, keluhan, kendaraan, tanggal_mulai, biaya, detail')
            //                 ->get();
            // return $dataDetails;       
        $detail=[];     
        foreach ($dataRekaps as $key => $value) {
            $data[$key]['id']=$value->id;
            $data[$key]['tanggal_pengajuan']=$value->tanggal_pengajuan;
            $data[$key]['nama_pegawai']=$value->nama_pegawai;
            $data[$key]['keluhan']=$value->keluhan;
            $data[$key]['kendaraan']=$value->kendaraan;
            $data[$key]['tanggal_mulai']=$value->tanggal_mulai;
            $data[$key]['tanggal_mulai']=$value->tanggal_mulai;
            $data[$key]['biaya']=$value->biaya;

            $dataDetails = DB::table('detail_service as ds')
                            ->leftJoin('m_service as ms','ms.id','=','ds.id_jenis_service')
                            ->where('id_rekap','=',$value->id)->get();
            
            $keys=0;

            foreach ($dataDetails as $keys => $item) {
                $detail[$keys]['nama_service'] = $item->nama_service;
                $detail[$keys]['harga'] = $item->harga;
            }
            $data[$key]['detail']=$detail;

        }

        $kirim['rekaps'] = $data; 
        return view('admin.rekap_print_excel',$kirim);
    }
}
