<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Pegawai;
use Hash;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|min:6'
        ]);
            
        $username = $request->input("username");
        $password = $request->input("password");
        $user = User::where("username", $username)->first();
        // return response()->json($user);
        
        if (!$user) {
            $out = [
                "message" => "login_failed",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
            // return view('auth.login')->with('flash_message_error','Username tidak ditemukan!.');
            // session::put('flash_message_error', 'Username tidak ditemukan!.');
            return redirect()->route('sendlogin')->with('flash_message_error','Username tidak ditemukan!.');
            return response()->json($out, $out['code']);
        }

        if (Hash::check($password, $user->password)) {
            // return 'ok';

            $dataUser = User::with(['pegawai'])
                    ->where("username", $username)
                    ->first();

            // return $dataUser;
            session::put('user_data', $dataUser);
            return redirect()->route('surat_home');

        } else {
            return redirect()->route('login')->with('error','Email-Address And Password Are Wrong.');
        }
    }

}
