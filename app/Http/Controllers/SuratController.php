<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Surat;
use App\Models\Pegawai;
use App\Models\MKategoriSurat;
use DataTables;
use Crypt;
use File;

class SuratController extends Controller
{
    public function index()
    {
        return view('admin.landing');
    }

    public function dataTable_surat(Request $request)
    {
        $query = Surat::leftjoin('m_kategori_surats as mks','mks.id','=','surats.id_kategori')
                        ->selectRaw("surats.*, mks.nama")
                        ->get();
        $data = array();
        $no=1;
        foreach ($query as $key => $value) {

            $link_unduh = asset('surat').'/'.$value->nama_file;
            $link_tambah = url('surat/create');
            $link_lihat = route('surat_lihat',[Crypt::encrypt($value->id)]);

            $buttonUnduh ='<a class="btn btn-xs btn-warning mr-1 editData" download href="'.$link_unduh.'"><i class="fa fa-file-download"></i>&nbsp; Unduh</a>';
            $buttonHapus = '<a class="btn btn-xs btn-danger mr-1 deleteData" data-id="'.$value->id.'" href="javascript:void(0)"><i class="far fa-trash-alt"></i>&nbsp; Hapus</a>';
            $buttonLihat ='<a class="btn btn-xs btn-primary mr-1" href="'.$link_lihat.'"><i class="far fa-eye"></i>&nbsp; Lihat >></a>';

            $data[$key]['no']=$no++;
            $data[$key]['id']=$value->id;
            $data[$key]['no_surat'] = $value->no_surat;
            $data[$key]['judul'] = $value->judul;
            $data[$key]['nama_file'] = $value->nama_file;
            $data[$key]['id_kategori'] = $value->id_kategori;
            $data[$key]['nama_kategori'] = $value->nama;
            $data[$key]['tgl_arsip'] = $value->created_at->locale('id')->toDateTimeString();   
            $data[$key]['aksi'] = $buttonHapus.' '.$buttonUnduh.' '.$buttonLihat;
        }   
        // dd($data);
        return DataTables::of($data)
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function create()
    {
        return view('admin.tambah');
    }

    public function select2Kategori(Request $request)
    {
        $term = trim($request->q);
        $search = strtolower($term);
        $data_send = MKategoriSurat::when($search , function ($query) use ($search ) {
                        if ($search) {
                            return $query->whereRaw("nama ilike '%".$search."%");
                        }
                    })
                    ->get();        

        return response()->json($data_send);
    }

    public function store(Request $request)
    {
        // dd($request->all());

        // $encrypted = Crypt::encrypt($_FILES['nama_file']['name']);
        // $decrypted_string = Crypt::decrypt($encrypted);            

        // dd(strlen($encrypted),$decrypted_string);

        $action = $request->action;
        $data['judul'] = $request->judul;
        $data['id_kategori'] = $request->kategori;
        $data['no_surat'] = $request->no_surat;

        if ($action == "tambah") {
            if (!empty($_FILES['nama_file']['name'])) {
                $file = $request->file('nama_file');
                $na   = Crypt::encrypt(date('Y-m-d H:i:s').'_'.$data['id_kategori']);
                $name = $na.'.'.$file->getClientOriginalExtension();
                $destinationPath =public_path('/surat/');;
                $file->move($destinationPath,$name);
                $data['nama_file']=$name;
            }
            try {
                Surat::create($data);
                return response()->json([
                    'kode' => 1,
                    'messages' => "Data berhasil disimpan!",
                ]);
            } catch (\Throwable $th) {
                return response()->json([
                    'kode' => 2,
                    'messages' => "Data gagal disimpan!",
                ]);
                //throw $th;
            }
            # code...
        } else {
            
            $surat = Surat::find($request->id_surat);
            $surat->judul = $data['judul'];
            $surat->id_kategori = $data['id_kategori'];
            $surat->no_surat = $data['no_surat'];            

            if (!empty($_FILES['nama_file']['name'])) {
                $url = public_path('surat');
                $file_ada = $url .'/'. $request->file_old;
                // dd($file_ada);
                File::delete($file_ada);                
            }

            if(!empty($_FILES['nama_file']['name'])) {
                $file = $request->file('nama_file');
                $na   = Crypt::encrypt(date('Y-m-d H:i:s').'_'.$data['id_kategori']);
                $name = $na.'.'.$file->getClientOriginalExtension();
                $destinationPath =public_path('/surat/');;
                $file->move($destinationPath,$name);
                $data['nama_file']=$name;
                $surat->nama_file = $data['nama_file'];
            }

            $surat->save();



            return response()->json([
                'kode' => 1,
                'messages' => "Data berhasil disimpan!",
            ]);            
            # code...
        }
    }

    public function show($id)
    {
        $data_surat = Surat::leftjoin('m_kategori_surats as mks', 'mks.id','=','surats.id_kategori')
                            ->selectRaw("surats.*, mks.nama")
                            ->where('surats.id',Crypt::decrypt($id))->first();
        $data['data_surat'] = $data_surat;
        return view('admin.lihat_edit',$data);
    }

    public function about(Request $request)
    {
        return view('admin.about');
    }

    public function destroy($id)
    {
        Surat::find($id)->delete();
    
        return response()->json([
            'kode' => 1,
            'messages' => "Data berhasil dihapus!",
        ]);   
    }
}
