<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $data_session = Session::get('user_data');

        if (isset($data_session)==null) {
            return redirect('/')->with('error',"You don't have admin access.");
        }
        
        $level = $data_session['level'];

        if($level == 'admin' || $level == 'kabag'){
            return $next($request);
        }

        return redirect('/')->with('error',"You don't have admin access.");
    }
}
