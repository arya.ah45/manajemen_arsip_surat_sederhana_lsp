-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `service_kendaraan_bea_cukai`;
CREATE DATABASE `service_kendaraan_bea_cukai` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `service_kendaraan_bea_cukai`;

DROP TABLE IF EXISTS `approvals`;
CREATE TABLE `approvals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(10) unsigned NOT NULL,
  `status` smallint(6) NOT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `approvals_id_pengajuan_foreign` (`id_pengajuan`),
  CONSTRAINT `approvals_id_pengajuan_foreign` FOREIGN KEY (`id_pengajuan`) REFERENCES `pengajuans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `approvals`;
INSERT INTO `approvals` (`id`, `id_pengajuan`, `status`, `catatan`, `created_by`, `created_at`, `updated_at`) VALUES
(155,	28,	1,	NULL,	'1931733021',	'2020-12-29 18:10:24',	'2020-12-29 18:10:24'),
(156,	28,	2,	NULL,	'1931733021',	'2020-12-29 18:11:12',	'2020-12-29 18:11:12'),
(157,	28,	4,	NULL,	'1931733001',	'2020-12-29 18:12:19',	'2020-12-29 18:12:19'),
(158,	28,	5,	NULL,	'1931733001',	'2020-12-29 18:12:25',	'2020-12-29 18:12:25'),
(159,	28,	6,	NULL,	'1931733001',	'2020-12-29 18:14:25',	'2020-12-29 18:14:25'),
(160,	28,	7,	NULL,	'1931733021',	'2020-12-29 18:22:54',	'2020-12-29 18:22:54');

DROP TABLE IF EXISTS `detail_service`;
CREATE TABLE `detail_service` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_rekap` int(10) unsigned NOT NULL,
  `id_service` int(10) unsigned NOT NULL,
  `id_jenis_service` smallint(5) unsigned NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_jenis_service` (`id_jenis_service`),
  KEY `id_rekap` (`id_rekap`),
  KEY `id_service` (`id_service`),
  CONSTRAINT `detail_service_ibfk_2` FOREIGN KEY (`id_jenis_service`) REFERENCES `m_service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_service_ibfk_4` FOREIGN KEY (`id_rekap`) REFERENCES `rekaps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_service_ibfk_6` FOREIGN KEY (`id_service`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `detail_service`;
INSERT INTO `detail_service` (`id`, `id_rekap`, `id_service`, `id_jenis_service`, `harga`, `created_at`, `updated_at`) VALUES
(8,	21,	22,	1,	5000000,	'2020-12-29 18:14:25',	'2020-12-29 18:14:25'),
(9,	21,	22,	4,	2500000,	'2020-12-29 18:14:25',	'2020-12-29 18:14:25');

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `failed_jobs`;

DROP TABLE IF EXISTS `kendaraans`;
CREATE TABLE `kendaraans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_kendaraan` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_merek` smallint(5) unsigned NOT NULL,
  `id_jenis` smallint(5) unsigned NOT NULL,
  `tipe` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warna` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_plat` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_sk` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_sk` date NOT NULL,
  `penanggungjawab` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kendaraans_id_merek_foreign` (`id_merek`),
  KEY `kendaraans_id_jenis_foreign` (`id_jenis`),
  KEY `kendaraans_penanggungjawab_foreign` (`penanggungjawab`),
  CONSTRAINT `kendaraans_id_jenis_foreign` FOREIGN KEY (`id_jenis`) REFERENCES `m_jenis_kendaraans` (`id`),
  CONSTRAINT `kendaraans_id_merek_foreign` FOREIGN KEY (`id_merek`) REFERENCES `m_merek_kendaraans` (`id`),
  CONSTRAINT `kendaraans_penanggungjawab_foreign` FOREIGN KEY (`penanggungjawab`) REFERENCES `pegawais` (`nip`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `kendaraans`;
INSERT INTO `kendaraans` (`id`, `nama_kendaraan`, `id_merek`, `id_jenis`, `tipe`, `warna`, `nomor_plat`, `nomor_sk`, `tanggal_sk`, `penanggungjawab`, `created_at`, `updated_at`) VALUES
(1,	'Ranger',	7,	2,	'XL',	'putih',	'AG 2312 BC',	'112',	'2020-01-01',	'1931733021',	NULL,	NULL),
(2,	'Avanza',	10,	1,	'E',	'hitam',	'AG 1123 BC',	'ok0-912',	'2020-01-01',	'1931733007',	NULL,	'2020-11-04 22:10:22'),
(3,	'Civic',	2,	1,	'RS',	'merah',	'AG 1 AS',	'SLk-1398-2312-123',	'2020-11-12',	'1931733117',	'2020-11-04 21:01:05',	'2020-11-04 21:01:05'),
(9,	'satria',	1,	3,	'fu',	'hijau',	'AG 1922 BC',	'SK-01-2020',	'2020-11-12',	NULL,	'2020-11-12 02:04:43',	'2020-11-12 02:04:43'),
(10,	'LC',	12,	5,	'Convertible',	'kuning',	'AG 619 REY',	'SK-123-619',	'2020-12-25',	'196702231998032021',	'2020-12-28 19:51:55',	'2020-12-28 19:51:55');

DROP TABLE IF EXISTS `kendaraan_fotos`;
CREATE TABLE `kendaraan_fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_kendaraan` int(10) unsigned NOT NULL,
  `nama_foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kendaraan_fotos_id_kendaraan_foreign` (`id_kendaraan`),
  CONSTRAINT `kendaraan_fotos_id_kendaraan_foreign` FOREIGN KEY (`id_kendaraan`) REFERENCES `kendaraans` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `kendaraan_fotos`;
INSERT INTO `kendaraan_fotos` (`id`, `id_kendaraan`, `nama_foto`, `created_at`, `updated_at`, `status`) VALUES
(8,	10,	'AG_619_REY_20201229034441.jpg',	'2020-12-28 20:44:41',	'2020-12-28 20:44:41',	0),
(9,	10,	'AG_619_REY_20201229035234.jpg',	'2020-12-28 20:52:34',	'2020-12-28 20:52:34',	0),
(11,	10,	'AG_619_REY_20201229050659.jpg',	'2020-12-28 22:06:59',	'2020-12-28 22:06:59',	1),
(12,	10,	'AG_619_REY_20201229050807.jpg',	'2020-12-28 22:08:07',	'2020-12-28 22:08:07',	2),
(13,	1,	'AG_2312_BC_20201230012430.jpg',	'2020-12-29 18:24:30',	'2020-12-29 18:24:30',	1),
(14,	1,	'AG_2312_BC_20201230012719.jpg',	'2020-12-29 18:27:19',	'2020-12-29 18:27:19',	2);

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `migrations`;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11,	'2010_09_23_043140_create_m_jenis_kendaraans_table',	1),
(12,	'2010_09_23_043207_create_m_merek_kendaraans_table',	1),
(13,	'2011_09_23_040934_create_pegawais_table',	1),
(14,	'2014_10_12_000000_create_users_table',	1),
(15,	'2014_10_12_100000_create_password_resets_table',	1),
(16,	'2019_08_19_000000_create_failed_jobs_table',	1),
(17,	'2020_09_23_040759_create_kendaraans_table',	1),
(18,	'2020_09_23_041104_create_pengajuans_table',	1),
(19,	'2020_09_23_041135_create_services_table',	1),
(20,	'2020_09_23_041201_create_rekaps_table',	1),
(21,	'2020_10_20_091159_create_approvals_table',	2),
(22,	'2020_10_24_052823_add_no_hp_column_to_pegawai_table',	3),
(23,	'2020_11_17_152357_create_kendaraan_fotos_table',	4),
(26,	'2020_11_18_024252_add_column_kendaraan_fotos_table',	5),
(27,	'2020_11_18_115902_add_column_id_rekaps_table',	5),
(30,	'2020_12_12_223520_create_m_service_table',	6),
(31,	'2020_12_12_234131_create_detail_service_table',	6);

DROP TABLE IF EXISTS `m_jenis_kendaraans`;
CREATE TABLE `m_jenis_kendaraans` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `m_jenis_kendaraans`;
INSERT INTO `m_jenis_kendaraans` (`id`, `nama_jenis`, `created_at`, `updated_at`) VALUES
(1,	'minibus',	'2020-10-30 06:13:15',	'2020-10-30 06:13:15'),
(2,	'Pick up',	'2020-10-30 06:13:15',	'2020-10-30 06:13:15'),
(3,	'Motor',	'2020-10-30 06:13:15',	'2020-10-30 06:13:15'),
(5,	'Sedan',	'2020-12-28 19:40:09',	'2020-12-28 19:40:09');

DROP TABLE IF EXISTS `m_merek_kendaraans`;
CREATE TABLE `m_merek_kendaraans` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nama_merek` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `m_merek_kendaraans`;
INSERT INTO `m_merek_kendaraans` (`id`, `nama_merek`, `created_at`, `updated_at`) VALUES
(1,	'Suzuki',	NULL,	NULL),
(2,	'Honda',	NULL,	NULL),
(3,	'Yamaha',	NULL,	NULL),
(7,	'Ford',	'2020-10-30 06:13:15',	'2020-10-30 06:13:15'),
(8,	'Chevrolet',	'2020-10-30 06:13:15',	'2020-10-30 06:13:15'),
(10,	'Toyota',	NULL,	NULL),
(12,	'Lexus',	'2020-12-28 19:31:17',	'2020-12-28 19:31:17');

DROP TABLE IF EXISTS `m_service`;
CREATE TABLE `m_service` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nama_service` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `m_service`;
INSERT INTO `m_service` (`id`, `nama_service`, `created_at`, `updated_at`) VALUES
(1,	'servis mesin',	NULL,	NULL),
(2,	'servis roda',	NULL,	NULL),
(3,	'servis rem',	NULL,	NULL),
(4,	'servis interior',	NULL,	NULL),
(5,	'servis rutin',	NULL,	NULL);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `password_resets`;

DROP TABLE IF EXISTS `pegawais`;
CREATE TABLE `pegawais` (
  `nip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pegawai` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bagian` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `no_hp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `pegawais`;
INSERT INTO `pegawais` (`nip`, `nama_pegawai`, `alamat`, `jabatan`, `bagian`, `created_at`, `updated_at`, `no_hp`) VALUES
('123',	'1234',	'123',	'123',	'123',	'2020-12-28 19:23:17',	'2020-12-28 19:24:01',	'123'),
('1931733001',	'Sindi Nur Kharisma',	'moro moro suko',	'Anggota',	'doggy div.',	NULL,	NULL,	'081358101509'),
('1931733007',	'Indah Wulansari',	'Bujel',	'Anggota',	'doggy div.',	NULL,	NULL,	'089666197630'),
('1931733021',	'arya hafizh tofani',	'Dsn. Grompol, Ds. ngebrak, Kediri',	'Ketua Kelompok',	'doggy div.',	NULL,	NULL,	'089666171252'),
('1931733117',	'Yanda Muhammad Naufall. A',	'Prambon Nganjuk',	'Anggota',	'doggy div.',	NULL,	NULL,	'082234363667'),
('196702231998032021',	'Rey Mysterio',	'Meksiko',	'Kepala Divisi',	'619',	'2020-12-28 19:12:14',	'2020-12-28 19:12:14',	'089666171253');

DROP TABLE IF EXISTS `pengajuans`;
CREATE TABLE `pengajuans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_kendaraan` int(10) unsigned NOT NULL,
  `keluhan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `srv_rutin` smallint(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pengajuans_id_kendaraan_foreign` (`id_kendaraan`),
  CONSTRAINT `pengajuans_id_kendaraan_foreign` FOREIGN KEY (`id_kendaraan`) REFERENCES `kendaraans` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `pengajuans`;
INSERT INTO `pengajuans` (`id`, `id_kendaraan`, `keluhan`, `srv_rutin`, `created_at`, `updated_at`) VALUES
(28,	1,	'interior sobek dan mesin suaranya berisik',	0,	'2020-12-29 18:10:24',	'2020-12-29 18:10:24');

DROP TABLE IF EXISTS `rekaps`;
CREATE TABLE `rekaps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_service` int(10) unsigned DEFAULT NULL,
  `tanggal_pengajuan` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `nama_pegawai` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keluhan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kendaraan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `detail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_kendaraan` int(10) unsigned DEFAULT NULL,
  `id_pegawai` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rekaps_id_pegawai_foreign` (`id_pegawai`),
  KEY `id_kendaraan` (`id_kendaraan`),
  KEY `id_service` (`id_service`),
  CONSTRAINT `rekaps_ibfk_1` FOREIGN KEY (`id_kendaraan`) REFERENCES `kendaraans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rekaps_ibfk_3` FOREIGN KEY (`id_service`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rekaps_id_pegawai_foreign` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawais` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `rekaps`;
INSERT INTO `rekaps` (`id`, `id_service`, `tanggal_pengajuan`, `nama_pegawai`, `keluhan`, `kendaraan`, `tanggal_mulai`, `tanggal_selesai`, `detail`, `biaya`, `created_at`, `updated_at`, `id_kendaraan`, `id_pegawai`) VALUES
(21,	22,	'2020-12-29 18:10:24',	'arya hafizh tofani',	'interior sobek dan mesin suaranya berisik',	'Ford Ranger',	'2020-12-30',	'2020-12-30',	NULL,	7500000,	'2020-12-29 18:14:25',	'2020-12-29 18:14:25',	1,	'1931733021');

DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(10) unsigned NOT NULL,
  `monitor` smallint(6) DEFAULT NULL,
  `tanggal_mulai` timestamp NULL DEFAULT NULL,
  `tanggal_selesai` timestamp NULL DEFAULT NULL,
  `detail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `services_id_pengajuan_foreign` (`id_pengajuan`),
  CONSTRAINT `services_id_pengajuan_foreign` FOREIGN KEY (`id_pengajuan`) REFERENCES `pengajuans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `services`;
INSERT INTO `services` (`id`, `id_pengajuan`, `monitor`, `tanggal_mulai`, `tanggal_selesai`, `detail`, `biaya`, `created_at`, `updated_at`) VALUES
(22,	28,	NULL,	'2020-12-29 18:12:25',	'2020-12-29 18:14:25',	NULL,	7500000,	'2020-12-29 18:12:19',	'2020-12-29 18:14:25');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_nip_foreign` (`nip`),
  CONSTRAINT `users_nip_foreign` FOREIGN KEY (`nip`) REFERENCES `pegawais` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE `users`;
INSERT INTO `users` (`id`, `username`, `password`, `level`, `nip`, `created_at`, `updated_at`) VALUES
(38,	'admin',	'$2y$10$pmywrZG1j5MUIqSDCnhP5.mvt7a/mgUVtZyWMfOhm3dsIXdWnIlwC',	'admin',	'1931733021',	'2020-10-23 23:58:41',	'2020-10-23 23:58:41'),
(39,	'verifikator',	'$2y$10$pJkCYs/lZlDuEZnaRMBrnuJWrsv3/OIlKhQsbMGm4laUkHeYAfB.O',	'verifikator',	'1931733001',	'2020-10-23 23:58:41',	'2020-10-23 23:58:41'),
(40,	'user',	'$2y$10$/pIJNlLIDCMr45aY6AnvR.dh2x.59Jar/6rHI4obf1rMtU2p.jGee',	'user',	'1931733007',	'2020-10-23 23:58:41',	'2020-10-23 23:58:41'),
(41,	'kabag',	'$2y$10$mLlrnQBTxQ5iEsbtmlNVjOuYk/p4MnYMCy0AXVmrfeFOegh194p1O',	'kabag',	'1931733117',	'2020-10-23 23:58:41',	'2020-10-23 23:58:41'),
(42,	'user2',	'$2y$10$pmywrZG1j5MUIqSDCnhP5.mvt7a/mgUVtZyWMfOhm3dsIXdWnIlwC',	'user',	'1931733021',	'2020-10-23 23:58:41',	'2020-10-23 23:58:41'),
(44,	'sadas',	'$2y$10$g6bauoun.Wy.HjfcznNTxu4Q8grqJunfuTXxWKpCDqXz2EgAiIk/u',	'verifikator',	'1931733007',	'2020-11-04 10:34:09',	'2020-11-04 10:34:46'),
(48,	'rey',	'$2y$10$Q3Lyss8yW2rE0XMvKJRHXuPUvDOavsDBEbwFSMFBsgBO1y8HZ3f.S',	'user',	'196702231998032021',	'2020-12-28 19:25:21',	'2020-12-28 19:25:21');

-- 2020-12-30 01:29:51
