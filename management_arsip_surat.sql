--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 11.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: m_kategori_surats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_kategori_surats (
    id integer NOT NULL,
    nama character varying(30) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.m_kategori_surats OWNER TO postgres;

--
-- Name: m_kategori_surats_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_kategori_surats_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_kategori_surats_id_seq OWNER TO postgres;

--
-- Name: m_kategori_surats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_kategori_surats_id_seq OWNED BY public.m_kategori_surats.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: pegawais; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pegawais (
    nip character varying(20) NOT NULL,
    nama_pegawai character varying(70) NOT NULL,
    alamat character varying(70) NOT NULL,
    jabatan character varying(20) NOT NULL,
    no_hp character varying(15) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.pegawais OWNER TO postgres;

--
-- Name: surats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.surats (
    id bigint NOT NULL,
    no_surat character varying(30) NOT NULL,
    judul character varying(70) NOT NULL,
    nama_file character varying NOT NULL,
    id_kategori integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.surats OWNER TO postgres;

--
-- Name: surats_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.surats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.surats_id_seq OWNER TO postgres;

--
-- Name: surats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.surats_id_seq OWNED BY public.surats.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying(20) NOT NULL,
    password character varying(255) NOT NULL,
    level character varying(15) NOT NULL,
    nip character varying(20),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: m_kategori_surats id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_kategori_surats ALTER COLUMN id SET DEFAULT nextval('public.m_kategori_surats_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: surats id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.surats ALTER COLUMN id SET DEFAULT nextval('public.surats_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: m_kategori_surats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_kategori_surats (id, nama, created_at, updated_at) FROM stdin;
1	Undangan	\N	\N
2	Pengumuman	\N	\N
3	Nota Dinas	\N	\N
4	Pemberitahuan	\N	\N
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2010_09_24_233906_create_m_kategori_surats_table	1
2	2011_09_23_040934_create_pegawais_table	1
3	2014_10_12_000000_create_users_table	1
4	2021_11_20_233645_create_surats_table	1
\.


--
-- Data for Name: pegawais; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pegawais (nip, nama_pegawai, alamat, jabatan, no_hp, created_at, updated_at) FROM stdin;
1931713021	arya hafizh tofani	Dsn. Grompol, Ds. ngebrak, Kediri	admin	089666171252	\N	\N
\.


--
-- Data for Name: surats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.surats (id, no_surat, judul, nama_file, id_kategori, created_at, updated_at) FROM stdin;
2	111/111/213.2/2021	Nota dinas Lapangan	eyJpdiI6Ijd2U0hMMlRuZ2dKQlZJdit1WG56UGc9PSIsInZhbHVlIjoiMUhuaExsbk1GajhWWDV5cFBadFlGR25NbnV5SGUzNVR6TDhBMVJUbko4QT0iLCJtYWMiOiJmMTlmOGQ2YWIyNzdhMTZkNGJmMWY2NzhhMDA2N2QxZGE3NjQzODE3NGM0MDQzMmJiZThhNmI4NGQ2MjQ2M2M3In0=.pdf	3	2021-11-21 20:31:35	2021-11-22 10:27:34
8	111/111/222/2021	Pemberitahuan Tes LSP 2021	eyJpdiI6IjlpL0s0NFNSK0wxTGtEeFkwZjNkOEE9PSIsInZhbHVlIjoiQkltUFYzVjhUM1ZTL0JlQjcra2M0OThRdC9tcFNReTJKNUc4T1VFREVUYz0iLCJtYWMiOiI3NDYzYmUzNDZhNTcyZTc1YzBjMjE3NjZjNGZmZWFiODRiYzI1NGI2NTU0NGFhYmM2M2FiMTk2YTBjZjBmNDFmIn0=.pdf	4	2021-11-23 20:41:57	2021-11-23 20:43:26
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, username, password, level, nip, created_at, updated_at) FROM stdin;
1	admin	$2y$10$GFgfy425cP2wQ.RF.BIhIuAqlbZC7lYLTuJe0aUSLgOtSMaE54Rg2	admin	1931713021	2021-11-21 00:09:23	2021-11-21 00:09:23
\.


--
-- Name: m_kategori_surats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_kategori_surats_id_seq', 4, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 4, true);


--
-- Name: surats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.surats_id_seq', 8, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: m_kategori_surats m_kategori_surats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_kategori_surats
    ADD CONSTRAINT m_kategori_surats_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: pegawais pegawais_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pegawais
    ADD CONSTRAINT pegawais_pkey PRIMARY KEY (nip);


--
-- Name: surats surats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.surats
    ADD CONSTRAINT surats_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: surats surats_id_kategori_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.surats
    ADD CONSTRAINT surats_id_kategori_foreign FOREIGN KEY (id_kategori) REFERENCES public.m_kategori_surats(id);


--
-- Name: users users_nip_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_nip_foreign FOREIGN KEY (nip) REFERENCES public.pegawais(nip) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

