<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use App\Models\User;

class dataUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = new User;
        $admin->username = 'admin';
        $admin->password = Hash::make('admin123');
        $admin->level = 'admin';
        $admin->nip = "1931713021";
        $admin->save();
        
    }
}
