<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class dataMKategoriSurat extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama' => 'Undangan',
            ],
            [
                'nama' => 'Pengumuman',
            ],
            [
                'nama' => 'Nota Dinas',
            ],
            [
                'nama' => 'Pemberitahuan',
            ],
        ];

        DB::table('m_kategori_surats')->insert($data);
    }
}
