<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class dataPegawai extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
        [
            'nip' => '1931713021',
            'nama_pegawai' => 'arya hafizh tofani',
            'alamat' => 'Dsn. Grompol, Ds. ngebrak, Kediri',
            'jabatan' => 'admin',
            'no_hp' => '089666171252'
        ],

        ];

        DB::table('pegawais')->insert($data);
    }
}
