<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surats', function (Blueprint $table) {
            $table->id();
            $table->string('no_surat',30); 
            $table->string('judul',70); 
            $table->string('nama_file',100); 
            $table->unsignedInteger('id_kategori');        
            $table->timestamps();

            $table->foreign('id_kategori')->references('id')->on('m_kategori_surats')->onDelete('NO ACTION')->onUpdate('NO ACTION');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surats');
    }
}
