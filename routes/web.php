<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {

    if (Session::has('user_data')) {
        return redirect()->route('surat_home');
    }else{
        return view('auth.login');
    }
});
Auth::routes();
Route::post('login', 'Auth\LoginController@login')->name('sendlogin');

#=================================================== A D M I N ===================================================
Route::group(['middleware' => ['admin']], function () {
    Route::prefix('surat')->group(function () {
        Route::get('/', "SuratController@index")->name('surat_home');
        Route::get('/lihat/{id}', "SuratController@show")->name('surat_lihat');
        Route::get('/delete/{id}', "SuratController@destroy")->name('surat_hapus');
        Route::get('/create', "SuratController@create");
        Route::get('dataTable_surat', "SuratController@dataTable_surat")->name('dataTable_surat');
        Route::get('select2Kategori', "SuratController@select2Kategori")->name('select2Kategori');
        Route::post('/simpan_surat', "SuratController@store")->name('simpan_surat');
    });

    Route::get('about', "SuratController@about")->name('about');
});
